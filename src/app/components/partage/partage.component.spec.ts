import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartageComponent } from './partage.component';

describe('PartageComponent', () => {
  let component: PartageComponent;
  let fixture: ComponentFixture<PartageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartageComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

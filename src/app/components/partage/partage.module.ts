import { PartageComponent } from 'src/app/components/partage/partage.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [PartageComponent],
  imports: [
    CommonModule
  ],
  exports:[PartageComponent]
})
export class PartageModule { }

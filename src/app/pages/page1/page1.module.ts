import { PartageModule } from './../../components/partage/partage.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Page1Page } from './page1.page';
import { PartageComponent } from 'src/app/components/partage/partage.component';

const routes: Routes = [
  {
    path: '',
    component: Page1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PartageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Page1Page]
})
export class Page1PageModule {}
